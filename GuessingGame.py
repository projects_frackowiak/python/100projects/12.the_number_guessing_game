import random
def verification(guess):
    global number
    if guess > number:
        return "To high!"
    elif guess < number:
        return "To low!"
    elif guess == number:
        return "Nice, you win"

def play_game(number):
    print("Welcome to the Number Guessing Game!")
    print("I'm thinking of a number between 1 and 100.")
    choose_difficult = input("Choose a difficulty. Type 'easy' or 'hard': ")

    if choose_difficult == 'easy':
        attempt = 10
    elif choose_difficult == 'hard':
        attempt = 5

    for x in range(attempt):
        print(f"You have {attempt - x} attempts remaining to guess the number")
        guess = int(input("Make a guess: "))
        print(verification(guess))
        if guess == number:
            break
    if guess != number:
        print("Good luck next time")

number = random.randint(1,100)
play_game(number)
if input("Wanna play again? Type 'y' or 'n': ") == 'y':
    number = random.randint(1,100)
    play_game(number)
else:
    print("Bye, Bye")